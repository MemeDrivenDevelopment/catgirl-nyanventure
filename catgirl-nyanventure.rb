require_relative "./interactible.rb"
require_relative "./player.rb"
require_relative "./catgirl.rb"
require_relative "./room.rb"
require_relative "./game.rb"

def extract(input)
  words = input.split(/\s+/).map(&:strip)
  
  case words.first
  when "exit"
    [:exit]
  when "look"
    [:look, words[1..-1].join(" ")]
  when "open"
    #[:open, *words[1..-1]]
    [:open, words[1..-1].join(" ")]
  when "where"
    [:where, words[1..-1].join(" ")]
  end
end

def prompt(text)
  loop do
    puts text
    print "> "
    input = gets.strip

    if input != ""
      return input
    else
      puts "Sorry, you did not provide any input."
    end
  end
end

game = Game.new(player_name: prompt("What is your name?"))

puts game.welcome

loop do
  input = prompt("What do you want to do next?")
  
  action, object = extract(input)
  
  case action
  when :exit
    puts "Thanks for playing!"
    exit
  when :look
    game.look(game.player, object)
  when :open
    game.open(game.player, object)
  when :where
    game.where(game.player, object)
  end
end
