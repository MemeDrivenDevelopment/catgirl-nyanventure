class Player
  attr_reader :name
  attr_accessor :location

  def initialize(game:, name:, location:)
    @game = game
    @name = name
    @location = location
  end
  
  def description
    "A handsome gentleperson."
  end

  def player?
    true
  end
end
