class Interactible
  attr_reader :name, :description, :openable

  def initialize(game:, name:, description:, openable:)
    @game = game
    @name = name
    @description = description
    @openable = openable
  end

  def open(actor)
    if @openable
      @game.trigger("open", interactible: self, actor: actor)
    end
  end

  def on_opened(interactible:, actor:)
    if interactible == self
      @game.print("#{actor.name} opened the #{interactible.name}")
    end
  end
end
