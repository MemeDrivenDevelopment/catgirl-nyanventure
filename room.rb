class Room
  attr_reader :name, :description

  def initialize(game:, name:, description:)
    @game = game
    @name = name
    @description = description
  end

  def on_entered(from:, to:, actor:)
    if to == self
      actor.location = self
      @game.print("#{actor.name} entered #{name} from #{from.name}")
    end
  end
end
