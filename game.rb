class Game
  attr_accessor :player, :catgirl

  def initialize(player_name:)
    @interactibles = {}
    @rooms = {}
    initialize_ecs
    @player = Player.new(game: self, name: player_name, location: self["living-room"])
    @catgirl = Catgirl.new(game: self, location: self["outside"])
  end
  
  def initialize_ecs
    @interactibles["front-door"] = Interactible.new(game: self, name: "Front Door", description: "Solidly made of the finest treated pine.", openable: true)
    @interactibles["bathroom-door"] = Interactible.new(game: self, name: "Bathroom Door", description: "Hollow but painted to look like it's not.", openable: true)
    @rooms["outside"] = Room.new(game: self, name: "The Outside", description: "A drab and desolate nightmare usually not filled with catgirls.")
    @rooms["living-room"] = Room.new(game: self, name: "The Living Room", description: "Home. Warmth. Comfort. TV. Catgirl News Network. A mug of cocoa, and a jelly dou(gh)nut.")
    @rooms["bathroom"] = Room.new(game: self, name: "The Bathroom", description: "Where the food cycle is completed.")
  end

  def [](name)
    @interactibles[name] || @rooms[name]
  end

  def welcome
    "You, #{@player.name}, are standing in a small, suburban house in the middle of the day. Around you is the quaint trappings of your livelihood - a couch, a tv, and a window out into the world. Beams of light flow into the room and fill you with a warm sense of belonging. You hear the faint sound of scratching at your door. What do you do?"
  end

  def open(actor, object)
    interactible = @interactibles.values.find { |interactible| object.downcase == interactible.name.downcase }
    #TODO: what if object not found?
    interactible.open(actor)
  end

  def look(actor, object_name)
    object = objects.find { |object| object_name.downcase == object.name.downcase }
    #TODO: what if object not found?
    #object.look(actor)
    puts object.description
  end

  def where(actor, entity_name)
    entity = entities.find { |entity| entity_name.downcase == entity.name.downcase }
    #TODO: what if object not found?
    puts "#{entity.name} is in #{entity.location.name}"
  end

  def trigger(event, **args)
    subscribers.each do |subscriber|
      subscriber.on(event, **args) if subscriber.respond_to?(:on)
      method_name = "on_#{event}ed"
      subscriber.send(method_name, **args) if subscriber.respond_to?(method_name)
    end
  end

  def subscribers
    objects
  end

  def objects
    @interactibles.values + @rooms.values + entities
  end

  def entities
    [@player, @catgirl]
  end

  def print(text)
    puts text
  end
end
