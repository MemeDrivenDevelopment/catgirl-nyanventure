class Catgirl
  attr_reader :name
  attr_accessor :location

  def initialize(game:, location:)
    @game = game
    @name = generate_name
    @location = location
  end

  def generate_name
    "Hazel"
  end

  def description
    "The most beautiful creature ever laid eyes upon."
  end

  def on_opened(interactible:, actor:)
    if interactible == @game["front-door"] && actor.player?
      @game.trigger("enter", from: @game["outside"], to: @game["living-room"], actor: self)
    end
  end

  def player?
    false
  end
end